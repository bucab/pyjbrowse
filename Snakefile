import getpass
import subprocess as sp
import sys

# the build command for singularity differs based on version
p = sp.run('singularity --version', stdout=sp.PIPE, shell=True)

if p.returncode != 0 :
  print('error running singularity, return code {}\n'.format(p.returncode),
    p.stdout,'\n',
    p.stderr,'\n',
    'check that it is installed and running properly')
  sys.exit(1)

singularity_version = p.stdout.decode().strip()
singularity_img = 'bubhub-jbrowse-{}.simg'.format(singularity_version)

rule all:
  input: singularity_img

rule base:
  input: 'base.Singularity'
  output: 'base.simg'
  params: username=getpass.getuser()
  shell:
    """
      SINGVER=`singularity --version`
      BIN=`which singularity`
      IMG="{output}"

      # build commands are different in 2.3 vs 2.4
      if [ "$SINGVER" == "2.3.2-dist" ];
      then
        $BIN create -F -s 2048 $IMG
        sudo $BIN bootstrap $IMG {input}
      else
        sudo $BIN build $IMG {input}
      fi
      sudo chown {params.username}:{params.username} $IMG
    """

rule jbrowse:
  input: base='base.simg',recipe='jbrowse.Singularity'
  output: singularity_img
  params: username=getpass.getuser()
  shell:
    """

      SINGVER=`singularity --version`
      BIN=`which singularity`
      IMG="{output}"

      # build commands are different in 2.3 vs 2.4
      if [ "$SINGVER" == "2.3.2-dist" ];
      then
        $BIN create -F -s 2048 $IMG
        sudo $BIN bootstrap $IMG {input.recipe}
      else
        sudo $BIN build $IMG {input.recipe}
      fi
      sudo chown {params.username}:{params.username} $IMG
    """
