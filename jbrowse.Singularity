Bootstrap: shub
From: singularityhub/ubuntu:latest

%help
echo "Usage: singularity run -B jbrowse:/jbrowse $SINGULARITY_NAME <jbrowse tool>.pl <jbrowse tool args>"
echo "Available tools:"
ls -1 /jbrowse_src/bin/
echo "The directory /jbrowse must be bound to an external user directory."
echo "e.g. mkdir jbrowse; singularity run -B jbrowse:/jbrowse ..."


%runscript
# make sure /jbrowse is writable, error if not
if [ ! -w /jbrowse ]; then
  echo "The directory /jbrowse must be bound to an external user directory."
  echo "e.g. mkdir jbrowse; singularity run -B jbrowse:/jbrowse ..."
  exit 1
fi

# copy /jbrowse_src into /jbrowse, which should be bound to a host directory
cp -ru /jbrowse_src/* /jbrowse/

if test -z "$1"; then
  echo "Usage: <jbrowse tool>.pl <jbrowse tool args>"
  echo "Available tools:"
  ls -1 /jbrowse/bin/
  exit 2
fi

# squelch perl complaining about locale settings it doesn't understand
LANG=C
LANGUAGE=$LANG
LC_ALL=$LANG
export LANG LANGUAGE LC_ALL

/jbrowse/bin/$@


%post
apt-get update --fix-missing
apt-get --no-install-recommends -y install \
    git build-essential zlib1g-dev \
    libxml2-dev libexpat-dev postgresql-client libpq-dev ca-certificates \
    nodejs wget unzip

# clear out old directories
rm -rf /jbrowse /cwd /jbrowse_src

VERSION=1.15.4
cd /
wget --no-clobber https://github.com/GMOD/jbrowse/releases/download/$VERSION-release/JBrowse-$VERSION.zip
unzip -n /JBrowse-$VERSION.zip
mv /JBrowse-$VERSION /jbrowse_src
cd /jbrowse_src/

./setup.sh &&
./bin/cpanm --force JSON Hash::Merge PerlIO::gzip Devel::Size \
    Heap::Simple Heap::Simple::XS List::MoreUtils Exception::Class Test::Warn \
    Bio::Perl Bio::DB::SeqFeature::Store File::Next Bio::DB::Das::Chado && \
rm -rf /root/.cpan/

# install gffread to convert gtf to gff
cd /
wget http://ccb.jhu.edu/software/stringtie/dl/gffread-0.9.10.Linux_x86_64.tar.gz -O gffread.tar.gz
tar zxf gffread.tar.gz
cp /gffread-0.9.10.Linux_x86_64/gffread /jbrowse_src/bin/

#perl Makefile.PL && make && make install

# this directory should be bound to a directory on the host
mkdir /jbrowse

# /cwd is for mounting the host current working
mkdir /cwd

# these are specific to scc
# create the directories in case OverlayFS support wasn't built in
for d in /scratch /share /project /projectnb /restricted /usr1 /usr2 /usr3 /usr4 /var/spool/sge /data;
do
  rm -rf $d
  mkdir $d
done
