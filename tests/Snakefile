from glob import glob

fa = 'saccharomyces_cerevisiae.fa'
gff = 'saccharomyces_cerevisiae.gff'
sizes = fa+'.sizes'
ind = 'R64-2_STAR_index'

samples = [_.replace('.fastq.gz','') for _ in glob('*.fastq.gz')]
bws = expand('{sample}__Signal.{m}.{str}.out.bw',
  sample=samples,
  m=('Unique','UniqueMultiple'),
  str=('str1','str2')
)
bams = expand('{sample}__Aligned.sortedByCoord.out.bam',sample=samples)
bais = expand('{sample}__Aligned.sortedByCoord.out.bam.bai',sample=samples)

vcfs = expand('{sample}.vcf.gz',sample=samples)

beds = expand('{sample}__roi.bed',sample=samples)

metadatas = dict(
    base='test_jbrowse_metadata_base__succ.csv',
    cols='test_jbrowse_metadata_w_addl_cols__succ.csv',
    unrec='test_jbrowse_metadata_unrec_datatrack__fail.csv'
)

tests = [_.replace('.csv','.test') for _ in metadatas.values()]

rule:
  input: bws, bais, vcfs, beds, 'metadata.done', tests

rule get_fa:
  output: temp('comb')
  shell:
    'wget https://downloads.yeastgenome.org/curation/chromosomal_feature/saccharomyces_cerevisiae.gff -O comb'

rule split_fa:
  input: 'comb'
  output:
    gff=gff,
    fa=fa
  shell:
    '''
    # for some stupid reason the top of the gff is gff and the bottom is fasta format
    csplit comb '/^>/'
    mv xx00 {output.gff}
    mv xx01 {output.fa}
    '''

# build fasta index and create sizes file
rule faidx:
  input: fa
  output: sizes
  shell:
    '''
    samtools faidx {input} &&
    cut -f1,2 {input}.fai > {output}
    '''

# build the genome index
rule star_ind:
  input: fa
  output: ind
  shell:
    'mkdir {output}; STAR --runMode genomeGenerate --genomeFastaFiles {input} --genomeDir {output} --genomeSAindexNbases 10'

# align reads against genome
rule star:
  input: reads='{sample}.fastq.gz', ind=ind
  output:
    bam='{sample}__Aligned.sortedByCoord.out.bam',
    bg1='{sample}__Signal.Unique.str1.out.bg',
    bg2='{sample}__Signal.Unique.str2.out.bg',
    bg3='{sample}__Signal.UniqueMultiple.str1.out.bg',
    bg4='{sample}__Signal.UniqueMultiple.str2.out.bg'
  params: prefix='{sample}__'
  shell:
    'STAR --runMode alignReads --readFilesIn={input.reads} --genomeDir={input.ind} --readFilesCommand zcat --outFileNamePrefix {params.prefix} --outSAMtype BAM SortedByCoordinate --outWigType bedGraph'

# create bam indices
rule bam_ind:
  input: '{sample}.bam'
  output: '{sample}.bam.bai'
  shell:
    'samtools index {input}'

rule vcf:
    input:
        bam='{sample}__Aligned.sortedByCoord.out.bam',
        fa=fa
    output: '{sample}.vcf.gz'
    shell:
        '''
        bcftools mpileup -o {output} -O z -f {input.fa} {input.bam}
        tabix -p vcf {output}
        '''

# convert bedgraph to bigwig
rule bg_to_bw:
  input: bg='{sample}.bg', sizes=sizes
  output: '{sample}.bw'
  shell:
    '''
    export LC_COLLATE=C;
    sort -k1,1 -k2,2n {input.bg} >tmp.bg;
    bedGraphToBigWig tmp.bg {input.sizes} {output};
    rm tmp.bg
    '''

rule random_beds:
    input:
        sizes=sizes
    output:
        '{sample}__roi.bed'
    shell:
        'bedtools random -seed 0 -l 100 -n 100 -g {input.sizes} > {output}'

rule create_metadata:
    input:
        fa=fa,
        gff=gff,
        bams=bams,
        bws=bws,
        vcfs=vcfs,
        beds=beds
    output:
        done=touch('metadata.done'),
        **metadatas
    run:
        import csv

        base_f = csv.writer(open(output.base,'w'))
        base_f.writerow(('dataType','trackType','trackLabel','srcPath'))
        base_f.writerows([
            ('fasta','refseq','S228_Genome','/cwd/{}'.format(input.fa)),
            ('gff','feature','R64-2_annotation','/cwd/{}'.format(input.gff))
        ])

        fieldnames = ('dataType','trackType','trackLabel','srcPath','category','style.color','style.pos_color','style.neg_color','style.arrowheadClass')
        cols_f = csv.DictWriter(open(output.cols,'w'),fieldnames)
        cols_f.writeheader()
        rows = [
            ('fasta','refseq','S228_Genome','/cwd/{}'.format(input.fa),'sequence','','null','',''),
            ('gff','feature','R64-2_annotation','/cwd/{}'.format(input.gff),'annotation','null','','')
        ]
        for row in rows :
            cols_f.writerow(dict(zip(fieldnames,row)))

        color_d = {k:v for k,v in zip(('1','2','3'),('#1b9e77','#d95f02','#7570b3'))}
        for bam in input.bams :
            sample = bam.split('__')[0]
            label = '{} BAM'.format(sample)
            path = '/cwd/{}'.format(bam)
            base = ('bam','reads',label,path)

            base_f.writerow(base)

            c = color_d[bam[bam.index('__')-1]]
            cols_f.writerow(dict(zip(fieldnames,base+('alignments',c,'null',c))))

        # duplicate the last bam to test old track skipping
        base_f.writerow(base)

        for bw in input.bws :

            sample = bw.split('__')[0]
            strand = 'bw+' if 'str1' in bw else 'bw-'
            single_multiple = 'Multi' if 'Multiple' in bw else 'Single'
            label = '{} {} {}'.format(sample, strand, single_multiple)
            path = '/cwd/{}'.format(bw)
            base = ('bw','quant',label,path)

            base_f.writerow(base)

            c = color_d[bw[bw.index('__')-1]]
            mol_type = 'DNA' if 'dna' in bw else 'RNA'
            cols_f.writerow(dict(zip(fieldnames,base+('{} wiggles'.format(mol_type),'',c))))

        for vcf in input.vcfs :

            sample = vcf.split('__')[0]
            label = '{} vcf'.format(sample)
            path = '/cwd/{}'.format(vcf)
            base = ('vcf','variant',label,path)

            base_f.writerow(base)

            cols_f.writerow(dict(zip(fieldnames,base+('intervals','green','nufin','','null'))))


        for bed in input.beds :

            sample = bed.split('__')[0]
            label = '{} bed'.format(sample)
            path = '/cwd/{}'.format(bed)
            base = ('bed','feature',label,path)

            base_f.writerow(base)

            c = color_d[bed[bed.index('__')-1]]
            cols_f.writerow(dict(zip(fieldnames,base+('intervals','green','nufin','','null'))))

        unrec_f = csv.writer(open(output.unrec,'w'))
        unrec_f.writerow(('dataType','trackType','trackLabel','srcPath','category','style.color'))
        unrec_f.writerows([
            ('fasta','junk','S228_Genome','/cwd/{}'.format(input.fa),'sequence',''),
            ('gff','feature','R64-2_annotation','/cwd/{}'.format(input.gff),'annotation','')
        ])


rule tests:
    input:
        'test_jbrowse_metadata_{test}__{state}.csv'
    output:
        out=touch('test_jbrowse_metadata_{test}__{state}.test'),
    params:
        dir='jbrowse_{test}' # this is a param because snakemake can't remove it without sudo
    shell:
        '''
            sudo rm -rf {params.dir} && mkdir {params.dir}
            OPTS="--rm --mount type=bind,source="$(pwd)",target=/cwd --mount type=bind,source="$(pwd)"/{params.dir},target=/jbrowse"
            case "{wildcards.state}" in
                succ)
                    docker run $OPTS pyjbrowse:latest build --debug /cwd/{input}
                    ;;
                fail)
                    docker run $OPTS pyjbrowse:latest build --debug /cwd/{input} || [ "$?" != "0" ]
                    ;;
            esac
        '''
