#!/usr/bin/env python3

'''
Usage:
    pyjbrowse.py [options] build [-o <dir>] <tracks_fn>
    pyjbrowse.py [options] exec <command>...

Options:
    -h --help            And stuff
    -o <dir>             Name of directory to store jbrowse output [default: jbrowse]
    -t FN --template=FN  JSON or .conf file that contains track templates
    --debug              Print debug messages on run
    --die-on-fail        Quit when exception encountered on any entry [default: False]
    --no-check-md5       Copy/symlink files into output directory without
                         checking if their md5 checksums match, otherwise only
                         copy if the checksums differ
    --force              process the records in the metadata file as if starting
                         from scratch, ignoring entries in trackList.json,
                         otherwise entry processing is skipped if the
                         trackLabel is found in the existing config,
                         implies --no-check-md5
    --symlink            Symlink files instead of copying
    --no-deploy          Don't try to deploy any files, just generate config
'''
import csv
from docopt import docopt
import json
import configparser
import hashlib
import gzip
import logging
import os
from pathlib import Path
from pprint import pformat
import re
import shutil
import subprocess as sp
import sys
import tempfile

#https://stackoverflow.com/questions/3232943/update-value-of-a-nested-dictionary-of-varying-depth
import collections
import six

# python 3.8+ compatibility
try:
    collectionsAbc = collections.abc
except:
    collectionsAbc = collections

def deep_update(d, u):
    for k, v in six.iteritems(u):
        dv = d.get(k, {})
        if not isinstance(dv, collectionsAbc.Mapping):
            d[k] = v
        elif isinstance(v, collectionsAbc.Mapping):
            d[k] = deep_update(dv, v)
        else:
            d[k] = v
    return d

def update_track_config(config, new_config) :

    # Look into config for 'tracks' array for object with the given label.
    # If it exists, update it with new_config, otherwise append it to 'tracks'

    label = new_config.get('label')
    labels = [_.get('label') for _ in config.get('tracks',[])]

    if label in labels : # already saw this label
        assert labels.count(label) == 1
        label_i = labels.index(label)
        conf_obj = config['tracks'][label_i]

        config['tracks'][label_i] = deep_update(conf_obj,new_config)

    else : # new label, append to end
        config['tracks'] = config.get('tracks',[])
        config['tracks'].append(new_config)

    return config

dbg = lambda *m: logging.debug(' '.join(str(_) for _ in m))
info = lambda *m: logging.info(' '.join(str(_) for _ in m))
warn = lambda *m: logging.warn(' '.join(str(_) for _ in m))
err = lambda *m: logging.error(' '.join(str(_) for _ in m))

def get_md5(fn) :
    hash_md5 = hashlib.md5()
    with open(fn, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b'') :
            hash_md5.update(chunk)
    checksum = hash_md5.hexdigest()
    dbg('.../{} md5 checksum: {}'.format(os.path.split(fn)[1],checksum))
    return checksum

def deploy_file(src,dest,nocheck=False,symlink=False) :
    '''Symlink/Copy *src* to *dest* only if the md5 hashes of the files don't match'''

    if symlink :
        if os.path.exists(dest) :
                # nocheck means delete the existing dest file and replace it
                if nocheck :
                        os.remove(dest)
                        os.symlink(src,dest)
                else :
                        # check that the existing symlink points to the same file as src
                        if os.path.realpath(dest) != os.path.realpath(src) :
                                err(('Symlink destination path already exists and points to a '
                                                'different file than source, aborting:\n'
                                                'Source: {}\n'
                                                'Dest: {} -> {}\n'
                                        ).format(os.path.realpath(src), dest, os.path.realpath(dest))
                                     )
                                sys.exit(1)
                        # else do nothing, symlink is ok as is
        else :
                os.symlink(src,dest)

    else : #copy
        if not os.path.exists(dest) or nocheck or get_md5(src) != get_md5(dest) :
            info('Copying .../{} to .../{}'.format(
                    os.path.split(src)[1],
                    os.path.split(dest)[1]
                )
            )
            shutil.copyfile(src,dest)
        else :
            info('Copy of .../{} to .../{} skipped'.format(
                    os.path.split(src)[1],
                    os.path.split(dest)[1]
                )
            )

def update_config(config,label,obj) :

    return config

def read_template_config(fn) :

    if fn is None :
        template_config = {}
    elif fn.endswith('.json') :
        with open(fn) as f :
            template_config = json.load(f)
    elif fn.endswith('.conf') :
        config = configparser.ConfigParser()
        template_config = config.read(fn)
    else :
        err('unsupported template format supplied, must be either JSON or '
            'readable by configparser module with appropriate extension '
            '(.json or .conf, respectively), aborting')
        sys.exit(1)

    return template_config

if __name__ == '__main__' :

    args = docopt(__doc__)

    if args['--debug'] :
        logging.basicConfig(level=logging.DEBUG)
    else :
        logging.basicConfig(level=logging.INFO)

    dbg('args')
    dbg(args)

    outdir = os.path.realpath(os.path.abspath(args['-o']))
    if not os.path.isdir(outdir) :
        info('creating outdir:',outdir)
        os.mkdir(outdir)

    datadir = os.path.join(outdir,'data')
    if not os.path.isdir(datadir) :
        info('creating datadir:',datadir)
        os.mkdir(datadir)

    sing_cmd = []

    # pass-through command
    if args['exec'] :
        cmd = sing_cmd + args['<command>']
        dbg('pass-through command:')
        dbg(cmd)
        sp.run(' '.join(cmd),shell=True)

    elif args['build'] :

        # load in templates, which is a dict with regular expressions as keys and
        # jbrowse config objects (dicts) as values
        templates = read_template_config(args['--template'])

        # replace the regex string keys with compiled Regex objects
        for k in list(templates.keys()) :
            regex = re.compile(k)
            templates[regex] = templates[k]
            del templates[k]

        config = {'tracks':[],'formatVersion':1}

        # read in the tracks.conf file if it exists
        conf_fn = os.path.join(datadir,'trackList.json')
        if os.path.exists(conf_fn) :
            with open(conf_fn) as f :
                config = json.load(f)

        dbg('Existing track labels:\n{}'.format(pformat([_['label'] for _ in config.get('tracks',[])])))

        known_data_track_types = set((
            ('bam','reads'),
            ('bw','quant'),
            ('fasta','refseq'),
            ('gff','feature'),
            ('gtf','feature'),
            ('bed','feature'),
            ('vcf','variant')
        ))

        with open(args['<tracks_fn>']) as f :

            dialect = csv.Sniffer().sniff(f.read(2048))
            f.seek(0)

            reader = csv.DictReader(f,dialect=dialect)

            expected_header = ['dataType', 'trackType', 'trackLabel', 'srcPath']

            if reader.fieldnames[:4] != expected_header :
                err('metadata file must have a header row with the following spec:\n'
                    'dataType,trackType,trackLabel,srcPath[,other,optional,fields,...]')
                sys.exit(1)

            addnl_headers = reader.fieldnames[4:]
            if len(addnl_headers) != 0 :
                info('found additional config fields in metadata:\n{}'.format(addnl_headers))

            metadata_trackLabels = set()

            # set to True only if any commands were run, i.e. a file wasn't skipped
            # this will trigger generating names
            cmd_ran = False

            for rec in reader :

                # some tracks don't need a command, if cmd remains None don't run one
                cmds = []

                dataType, trackType, trackLabel, srcPath = [rec[_] for _ in expected_header]

                # skip rows
                if dataType.startswith('#') :
                    continue

                if trackLabel in metadata_trackLabels :
                    warn(('There were multiple metadata records with the same '
                           'trackLabel="{}", only the last record with this '
                           'trackLabel will be reflected in the config'
                          ).format(trackLabel)
                    )
                metadata_trackLabels.add(trackLabel)

                # preexisting trackLabels
                prev_trackLabels = set([_['label'] for _ in config.get('tracks',[])])

                new_track = trackLabel not in prev_trackLabels

                if not new_track :
                    info(('track with label {} previously processed, updating '
                         'config only').format(trackLabel)
                     )

                if (dataType, trackType) not in known_data_track_types :
                    err(('I do not recognize this dataType/trackType combo: '
                        '({},{}), I understand these:\n{}'.format(dataType,trackType,pformat(known_data_track_types)))
                       )
                    sys.exit(1)

                # try to match rec to template using trackLabel
                matching_templates = []
                for regex in templates :
                    if regex.search(rec['trackLabel']) is not None :
                        matching_templates.append(regex)
                if len(matching_templates) > 1 :
                    err(('more than one template matched the trackLabel={}:\n'
                        '{}\neach track must match at most one template, aborting')
                        .format(rec['trackLabel'],[_.pattern for _ in matching_templates])
                       )
                    sys.exit(1)

                tmpl = {}
                if len(matching_templates) != 0 :
                    tmpl = templates[matching_templates[0]]

                    # the template can have any of the columns in the metadata file
                    # substituted in, e.g. "some_key" : "{trackLabel}"
                    for k,v in tmpl.items() :
                        tmpl[k] = v.format(**rec)

                # override any template fields with those in the metadata file
                def nested_dict(k,v) :
                    if '.' not in k :
                        if v == 'null' : # replace the string null with None
                            v = None
                        return {k:v}
                    else :
                        k1,k2 = k.split('.',1)
                        return { k1 : nested_dict(k2,v) }

                for fld in addnl_headers :
                    # nested objects, e.g. "style": { "color": "red" }
                    # are encoded with dot notation in the metadata header, e.g.
                    # style.color

                    # only update template if the value is not empty
                    if len(rec[fld]) > 0 :
                        deep_update(tmpl, nested_dict(fld,rec[fld]))

                info('Processing:',rec)

                # check for source path existence
                if not os.path.exists(srcPath) :
                    msg = 'srcPath {} does not exist'.format(srcPath)
                    if args['--die-on-fail'] :
                        err(msg)
                        sys.exit(1)
                    else :
                        warn(msg)
                        continue

                # if the given path is relative, add a '/cwd/' for the cwd mount
                # otherwise just pass the path through
                cmdPath = srcPath if os.path.isabs(srcPath) else '/cwd/'+srcPath
                basename = os.path.split(srcPath)[1]
                dataPath = os.path.join(datadir,basename)

                new_config = None

                if trackType == 'refseq' : # prepare-refseqs.pl

                    #prepare-refseqs.pl --gff <GFF file>    [options]
                    #prepare-refseqs.pl --fasta <file1> --fasta <file2>    [options]
                    #prepare-refseqs.pl --indexed_fasta <file>    [options]
                    #prepare-refseqs.pl --twobit <file>    [options]
                    #prepare-refseqs.pl --conf <JBrowse config file>    [options]
                    #prepare-refseqs.pl --sizes <sizes file>    [options]

                    if dataType == 'fasta' :
                        cmds.append(['prepare-refseqs.pl','--out '+datadir,
                            '--fasta '+cmdPath,
                            '--trackLabel "{}"'.format(trackLabel)
                        ])
                        new_config = {
                             "category" : "Reference sequence",
                             "key" : "Reference sequence",
                             "label" : trackLabel,
                             "seqType" : "dna",
                             "storeClass" : "JBrowse/Store/Sequence/StaticChunked",
                             "type" : "SequenceTrack",
                             "urlTemplate" : "seq/{refseq_dirpath}/{refseq}-"
                          }

                    else :
                        msg = ('Unrecognized/unimplemented dataType for '
                            'tracktype==refseq: {}\n'.format(dataType))
                        if args['--die-on-fail'] :
                            err(msg)
                            sys.exit(1)
                        else :
                            warn(msg)
                            continue

                elif trackType == 'feature' :

                    #flatfile-to-json.pl                                                                                                                 \
                    #            ( --gff <GFF3 file> | --bed <BED file> | --gbk <GenBank file> )                 \
                    #            --trackLabel <track identifier>                                                                                 \
                    #            [ --trackType <JS Class> ]                                                                                            \
                    #            [ --out <output directory> ]                                                                                        \
                    #            [ --key <human-readable track name> ]                                                                     \
                    #            [ --className <CSS class name for displaying features> ]                                \
                    #            [ --urltemplate "http://example.com/idlookup?id={id}" ]                                 \
                    #            [ --arrowheadClass <CSS class> ]                                                                                \
                    #            [ --noSubfeatures ]                                                                                                         \
                    #            [ --subfeatureClasses '{ JSON-format subfeature class map }' ]                    \
                    #            [ --clientConfig '{ JSON-format style configuration for this track }' ] \
                    #            [ --config '{ JSON-format extra configuration for this track }' ]             \
                    #            [ --thinType <BAM -thin_type> ]                                                                                 \
                    #            [ --thicktype <BAM -thick_type>]                                                                                \
                    #            [ --type <feature types to process> ]                                                                     \
                    #            [ --nclChunk <chunk size for generated NCLs> ]                                                    \
                    #            [ --compress ]                                                                                                                    \
                    #            [ --sortMem <memory in bytes to use for sorting> ]                                            \
                    #            [ --maxLookback <maximum number of features to buffer in gff3 files> ]    \
                    #            [ --nameAttributes "name,alias,id" ]                                                                        \

                    # config is the same for all feature track types
                    new_config = {
                         'key' : trackLabel,
                         'label' : trackLabel,
                    }

                    if dataType in ('gff','gtf') :

                        # jbrowse doesn't support gtf, so we need to convert it to gff with
                        # gffread
                        if dataType == 'gtf' :
                            gtf = os.path.split(cmdPath)[1]
                            gff = os.path.join(datadir,gtf.replace('.gtf','.gff'))
                            gff_cmd = sing_cmd+['gffread -EG {gtf} -o {gff}'.format(
                                gtf=cmdPath,
                                gff=gff
                                )
                            ]
                            #info('converting gtf to gff')
                            #dbg(' '.join(gff_cmd))
                            #sp.run(' '.join(gff_cmd),
                            #    shell=True
                            #)
                            cmds.append(gff_cmd)
                            cmdPath = gff

                        cmds.append(['flatfile-to-json.pl','--out '+datadir,'--gff '+cmdPath,
                            '--trackLabel "{}"'.format(trackLabel)
                        ])

                    elif dataType == 'bed' :

                        cmds.append(['flatfile-to-json.pl','--out '+datadir,'--bed '+cmdPath,
                            '--trackLabel "{}"'.format(trackLabel)
                        ])

                    else :

                        msg = ('Unrecognized/unimplemented dataType for '
                            'tracktype==feature: {}\n'.format(dataType))
                        if args['--die-on-fail'] :
                            err(msg)
                            sys.exit(1)
                        else :
                            warn(msg)

                elif trackType == 'reads' :

                    '''
                    {
                         "type" : "JBrowse/View/Track/Alignments2",
                         "urlTemplate" : "sacCer_dnaseq_1__Aligned.sortedByCoord.out.bam",
                         "label" : "sacCer_dnaseq_1",
                         "storeClass" : "JBrowse/Store/SeqFeature/BAM",
                         "key" : "sacCer_dnaseq_1"
                    },
                    {
                         "type" : "JBrowse/View/Track/Alignments2",
                         "urlTemplate" : "sacCer_dnaseq_1__Aligned.sortedByCoord.out.cram",
                         "label" : "sacCer_dnaseq_1",
                         "storeClass" : "JBrowse/Store/SeqFeature/CRAM",
                         "key" : "sacCer_dnaseq_1"
                    },

                    '''

                    if dataType in ('bam','cram') :
                        # copy the bam/cram into the data dir
                        try :
                            if not args['--no-deploy'] :
                                deploy_file(srcPath,dataPath,args['--no-check-md5'],symlink=args['--symlink'])
                        except Exception as e:
                            msg = ('Error copying file {}: {}'.format(srcPath,e.args))
                            if args['--die-on-fail'] :
                                err(msg)
                                sys.exit(2)
                            else :
                                warn(msg)
                                continue

                        # try to copy the index into the data dir, fail if it cannot be found
                        indexSrcPath = srcPath+'.bai' if dataType == 'bam' else srcPath+'.crai'
                        if not os.path.exists(indexSrcPath) :
                            msg = ('No .bai/.crai file was found next to .bam file, aborting\n{}'.format(srcPath))
                            if args['--die-on-fail'] :
                                err(msg)
                                sys.exit(2)
                            else :
                                warn(msg)
                                continue

                        indexBasename = os.path.split(indexSrcPath)[1]
                        indexDataPath = os.path.join(datadir,indexBasename)
                        if not args['--no-deploy'] :
                            deploy_file(indexSrcPath,indexDataPath,args['--no-check-md5'],symlink=args['--symlink'])

                        new_config = {
                                'storeClass': 'JBrowse/Store/SeqFeature/{}'.format(dataType.upper()),
                                'urlTemplate': basename,
                                'category': 'NGS',
                                'type': 'JBrowse/View/Track/Alignments2',
                                'label': trackLabel,
                                'autoscale': 'local',
                                'key': trackLabel
                        }

                        #cmd = ['add-bam-track.pl --label','"{}"'.format(trackLabel),
                        #    '--bam_url',basename,'--in',conf_fn
                        #]

                elif trackType == 'quant' :

                    '''
                    {
                         "type" : "JBrowse/View/Track/Wiggle/Density",
                         "bicolor_pivot" : "zero",
                         "storeClass" : "JBrowse/Store/SeqFeature/BigWig",
                         "key" : "sacCer_mrnaseq_3__Signal.Unique.str2.out.bw",
                         "urlTemplate" : "sacCer_mrnaseq_3__Signal.Unique.str2.out.bw",
                         "autoscale" : "local",
                         "label" : "sacCer_mrnaseq_3__Signal.Unique.str2.out.bw"
                    }
                    '''

                    if dataType == 'bw' :
                        # copy the bw into the data dir
                        if not args['--no-deploy'] :
                            deploy_file(srcPath,dataPath,args['--no-check-md5'],
                                symlink=args['--symlink']
                            )

                        new_config = {
                                'type': 'JBrowse/View/Track/Wiggle/XYPlot',
                                'bicolor_pivot': 'zero',
                                'storeClass': 'JBrowse/Store/SeqFeature/BigWig',
                                'key': trackLabel,
                                'urlTemplate': basename,
                                'category': 'WIG',
                                'autoscale': 'local',
                                'label': trackLabel
                        }

                        #cmd = ['add-bw-track.pl --label', '"{}"'.format(trackLabel),
                        #    '--bw_url',basename,'--in',conf_fn
                        #]

                elif trackType == 'variant' :

                    if dataType == 'vcf' :
                        # VCFs must be bgzipped and tabix indexed
                        # check for gzip
                        try :
                            with gzip.open(srcPath,'rb') as f :
                                f.read(10)
                        except OSError :
                            err('VCF files must be bgzipped to be displayed in jbrowse, '
                                'do this prior to building with pyjbrowse.')
                            sys.exit(1)

                        # check to see if {srcPath}.tbi exists
                        tbi = Path(str(srcPath) + '.tbi')
                        if not tbi.exists() :
                            err('VCF files must be tabix-indexed, could not find the file '
                                    + str(tbi) +', do this prior to building with pyjbrowse')
                            sys.exit(1)

                        # copy the vcf and tabix into the staging directory
                        deploy_file(srcPath,dataPath,args['--no-check-md5'],
                                symlink=args['--symlink']
                        )

                        tbiDataPath = os.path.join(datadir,tbi.name)
                        deploy_file(str(tbi),tbiDataPath,args['--no-check-md5'],
                                symlink=args['--symlink']
                        )

                        new_config = {
                                'type': 'JBrowse/View/Track/HTMLVariants',
                                'storeClass': 'JBrowse/Store/SeqFeature/VCFTabix',
                                'key': trackLabel,
                                'urlTemplate': basename,
                                'category': 'WIG',
                                'autoscale': 'local',
                                'label': trackLabel
                        }
                        new_config.update(tmpl)

                dbg('trackLabel {} new track == {}'.format(trackLabel,new_track))
                dbg('cmds',pformat(cmds))

                if new_config is not None :
                    new_config = deep_update(new_config,tmpl)
                    config = update_track_config(config, new_config)

                if len(cmds) > 0 and (args['--force'] or new_track) :

                    cmd_ran = True

                    # some processsing steps have multiple commands
                    for cmd in cmds :

                        # need to write all the current config to file before running
                        # command since the command might change it
                        with open(conf_fn,'w') as f :
                            dbg('dumping config')
                            json.dump(config,f,indent=2)

                        cmd = sing_cmd+cmd
                        dbg('jbrowse command:')
                        dbg(' '.join(cmd))
                        sp.run(' '.join(cmd),shell=True)

                        # then need to read config again
                        with open(conf_fn) as f :
                            dbg('re-reading config')
                            jb_config = json.load(f)

                            # jbrowse scripts might have added stuff to the config
                            # sync up the tracks
                            for track in config.get('tracks',[]) :
                                config = update_track_config(jb_config, track)

                else :
                    reasons = []
                    if len(cmds) == 0 :
                        reasons.append('no commands to run')
                    if args['--force'] :
                        reasons.append('--force supplied')
                    if not new_track :
                        reasons.append('old track')

                    dbg('command not run because: {}'.format(reasons))

            # write out the final config
            with open(conf_fn,'w') as f :
                info('writing final config to',conf_fn)
                json.dump(config,f,indent=2)

            # only run generate names after everything is done if we ran a
            # command that might require regenerating names
            if cmd_ran :
                gen_cmd = sing_cmd+['generate-names.pl --verbose','--incremental','--hashBits 16','--out {}'.format(datadir)]
                info('running final generate-names.pl')
                dbg('generate-names.pl command:')
                dbg(' '.join(gen_cmd))
                sp.run(' '.join(gen_cmd),
                    shell=True
                )
