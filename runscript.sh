#!/bin/bash

# make sure /jbrowse is writable, error if not
if [ ! -w /jbrowse ]; then
  echo "The directory /jbrowse must be bound to an external user directory."
  echo "e.g. mkdir jbrowse; singularity run -B jbrowse:/jbrowse ..."
  exit 1
fi

# copy /jbrowse_src into /jbrowse, which should be bound to a host directory
cp -ru /jbrowse_src/* /jbrowse/

# squelch perl complaining about locale settings it doesn't understand
LANG=C
LANGUAGE=$LANG
LC_ALL=$LANG
export LANG LANGUAGE LC_ALL

pyjbrowse.py $@
