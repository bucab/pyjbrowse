set +x
docker run --rm -d -p 5000:5000 --name registry registry:2
# give the registry a second to start up properly
sleep 1

docker tag pyjbrowse localhost:5000/pyjbrowse
docker push localhost:5000/pyjbrowse

curl http://localhost:5000/v2/pyjbrowse/tags/list

SINGULARITY_DOCKER_REGISTRY='--registry localhost:5000'
SINGULARTY_NOHTTPS='yes'
export SINGULARITY_DOCKER_REGISTRY SINGULARTY_NOHTTPS
SINGULARITY_NOHTTPS=1 singularity build pyjbrowse-latest.simg docker://localhost:5000/pyjbrowse:latest

docker stop registry
